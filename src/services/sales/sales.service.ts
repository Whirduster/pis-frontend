import {Injectable} from '@angular/core';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  AverageUtilization,
  MenuItemSalesLastTime,
  SoldMenuItem
} from '../../app/sales/sales/sales.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';

@Injectable({
  providedIn: 'root'
})

export class SalesService {
  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  getCountOfSoldMenuItems(): Observable<[SoldMenuItem]> {
    return this.http.get<any>(this.urlRoutesConstants.GET_COUNT_OF_SOLD_MENU_ITEMS_URL, HeaderSetterUtil.setAuthorization());
  }

  getLastTimeWhenMenuItemWasSold(): Observable<[MenuItemSalesLastTime]> {
    return this.http.get<any>(this.urlRoutesConstants.GET_LAST_TIME_WHEN_MENU_ITEM_WAS_SOLD_URL, HeaderSetterUtil.setAuthorization());
  }

  getAverageUtilization(): Observable<AverageUtilization[]> {
    return this.http.get<any>(this.urlRoutesConstants.GET_UTILIZATION, HeaderSetterUtil.setAuthorization());
  }

  getSumOfSalesOfPeriod(year: number, month: number): Observable<number> {
    return this.http.get<any>(this.urlRoutesConstants.SALES + year + '\\' + month, HeaderSetterUtil.setAuthorization());
  }

}
