import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegisterModel} from '../../app/unlogged-user-pages/register/register.model';
import {Observable} from 'rxjs';
import {TokenResponseModel} from '../../app/response.model';
import {AuthenticationConstants} from '../../app/authentication/authentication.constants';
import {LoginModel} from '../../app/unlogged-user-pages/login/login.model';
import {LoginResponseModel} from '../../app/response.model';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {Md5} from 'ts-md5';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';
import {EditProfileModel} from '../../app/authentication/edit-profile/edit-profile.model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public readonly authConstants: typeof AuthenticationConstants = AuthenticationConstants;
  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  sendRegisterData(userModel: RegisterModel): Observable<TokenResponseModel> {
    const userWithHashedPass: RegisterModel = {
      name: userModel.name,
      surname: userModel.surname,
      email: userModel.email,
      password: this.hashPassword(userModel.password)
    };
    return this.http.post<any>(this.urlRoutesConstants.REGISTER_URL, userWithHashedPass);
  }

  sendLoginData(loginModel: LoginModel): Observable<LoginResponseModel> {
    const userWithHashedPass: LoginModel = {
      email: loginModel.email,
      password: this.hashPassword(loginModel.password)
  };
    return this.http.post<any>(this.urlRoutesConstants.LOGIN_URL, userWithHashedPass);
  }

  hasUserSpecificRole(roleName: string): boolean {
    const role = this.authConstants.USER_NAME_HASH_ARRAY.find(s => s.name === roleName);
    return !! (localStorage.getItem(role.name) === role.hash); // operators !! are necessary, they make false of null
  }

  isUserNotLoggedIn(): boolean {
    return !this.isEmployeeLoggedIn() && !this.hasUserSpecificRole(this.authConstants.LOGGED_USER_NAME);
  }

  isEmployeeLoggedIn(): boolean {
    return this.hasUserSpecificRole(this.authConstants.MANAGER_NAME) ||
           this.hasUserSpecificRole(this.authConstants.CHEF_NAME)    ||
           this.hasUserSpecificRole(this.authConstants.COOK_NAME)    ||
           this.hasUserSpecificRole(this.authConstants.WAITER_NAME);
  }

  public hashPassword(password: string): string {
    return Md5.hashStr(password).toString();
  }

  getUserData(): Observable<EditProfileModel> {
    return this.http.get<any>(this.urlRoutesConstants.EDIT_PROFILE_GET, HeaderSetterUtil.setAuthorization());
  }

  updateUserData(editProfileModel: EditProfileModel, email): Observable<TokenResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.EDIT_PROFILE_PUT + email,
      editProfileModel, HeaderSetterUtil.setAuthorization());
  }

  getHomepage(router: Router) {
    if (this.hasUserSpecificRole(this.authConstants.MANAGER_NAME)) {
      router.navigate(['/sales']);
    } else if (this.hasUserSpecificRole(this.authConstants.CHEF_NAME)) {
      router.navigate(['/order-items']);
    } else if (this.hasUserSpecificRole(this.authConstants.COOK_NAME)) {
      router.navigate(['/order-items']);
    } else if (this.hasUserSpecificRole(this.authConstants.WAITER_NAME)) {
      router.navigate(['/restaurant-scheme']);
    } else if (this.hasUserSpecificRole(this.authConstants.LOGGED_USER_NAME)) {
      router.navigate(['/homepage']);
    } else {
      router.navigate(['/homepage']);
    }
  }
}
