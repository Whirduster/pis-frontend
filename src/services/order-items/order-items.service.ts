import { Injectable } from '@angular/core';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OrderItem} from '../../app/order-items/order-items.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';
import {OrderItemsOfPlaceResponseModel, ResponseModel, ResponseModelWithId} from '../../app/response.model';

@Injectable({
  providedIn: 'root'
})
export class OrderItemsService {

  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {
  }

  getOrderItemsListData(): Observable<[OrderItem]> {
    return this.http.get<any>(this.urlRoutesConstants.ORDER_ITEMS, HeaderSetterUtil.setAuthorization());
  }

  markOrderAsPreparing(orderItemId: number): Observable<ResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.ORDER_ITEMS + orderItemId +
      this.urlRoutesConstants.PREPARING, {}, HeaderSetterUtil.setAuthorization());
  }

  markOrderAsFinished(orderItemId: number): Observable<ResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.ORDER_ITEMS + orderItemId +
      this.urlRoutesConstants.FINISHED, {},  HeaderSetterUtil.setAuthorization());
  }

  markOrderAsReleased(orderItemId: number): Observable<ResponseModel> {
    return this.http.put<any>(this.urlRoutesConstants.ORDER_ITEMS + orderItemId +
      this.urlRoutesConstants.RELEASED, {}, HeaderSetterUtil.setAuthorization());
  }

  getOrderAndMenuItemsInCategories(placeId: number): Observable<OrderItemsOfPlaceResponseModel> {
    return this.http.get<any>(this.urlRoutesConstants.PLACES + placeId +
      this.urlRoutesConstants.ADD_ORDER_ITEM, HeaderSetterUtil.setAuthorization());
  }

  deleteOrderItem(placeId: number, orderItemId: number): Observable<ResponseModel> {
    return this.http.delete<any>(this.urlRoutesConstants.PLACES + placeId +
      this.urlRoutesConstants.WAITER_ORDER_ITEMS + orderItemId, HeaderSetterUtil.setAuthorization());
  }

  addOrderItemToTableOrder(placeId: number, menuItemId: number): Observable<ResponseModelWithId> {
    return this.http.post<any>(this.urlRoutesConstants.PLACES + placeId +
      this.urlRoutesConstants.ADD_ORDER_ITEM + menuItemId, {},  HeaderSetterUtil.setAuthorization());
  }

  addSpecialRequirement(placeId: number, orderItemId: number, requirements: string): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.PLACES + placeId +
      this.urlRoutesConstants.WAITER_ORDER_ITEMS + orderItemId + this.urlRoutesConstants.REQUIREMENT,
      { specialRequirements: requirements}, HeaderSetterUtil.setAuthorization());
  }

}
