import { Injectable } from '@angular/core';
import {GetFreeAndOccupiedTablesResponse} from '../../app/response.model';
import {
  ReservationInfo
} from '../../app/reservation/create-reservation/create-reservation.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationDataStorage {
  public data: {
    places: GetFreeAndOccupiedTablesResponse;
    reservationInfo: ReservationInfo;
  };

  constructor() { }

  public fillDataStorage(places: GetFreeAndOccupiedTablesResponse, reservationInfo: ReservationInfo) {
    this.data = {
      places,
      reservationInfo
    };
  }
}
