import {Injectable} from '@angular/core';
import {AuthenticationConstants} from '../../app/authentication/authentication.constants';
import {AuthenticationService} from '../authentication/authentication.service';

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  sidebarBackground: string;
  showInSidebar: Array<string>;
  showInNavbar: Array<string>;
}

@Injectable({
  providedIn: 'root'
})
export class NavigationRoutesService {
  routes = [
    {
      path: '/account',
      title: 'Účet',
      icon: 'fas fa-user-circle',
      sidebarBackground: 'assets/img/sidebar-homepage.jpg',
      showInSidebar: Array(),
      showInNavbar: AuthenticationConstants.EMPLOYEE_NAMES.concat(AuthenticationConstants.LOGGED_USER_NAME)
    },
    {
      path: '/register',
      title: 'Registrace',
      icon: 'fas fa-user-plus',
      sidebarBackground: 'assets/img/sidebar-homepage.jpg',
      showInSidebar: Array(),
      showInNavbar: Array(AuthenticationConstants.UNLOGGED_USER_NAME)
    },
    {
      path: '/login',
      title: 'Přihlášení',
      icon: 'fas fa-sign-in-alt',
      sidebarBackground: 'assets/img/sidebar-homepage.jpg',
      showInSidebar: Array(),
      showInNavbar: Array(AuthenticationConstants.UNLOGGED_USER_NAME)
    },
    {
      path: '/login',
      title: 'Odhlášení',
      icon: 'fas fa-sign-out-alt',
      sidebarBackground: 'assets/img/sidebar-homepage.jpg',
      showInSidebar: Array(),
      showInNavbar: AuthenticationConstants.EMPLOYEE_NAMES.concat(AuthenticationConstants.LOGGED_USER_NAME)
    },
    {
      path: '/restaurant-scheme',
      title: 'Schéma restaurace',
      icon: 'fas fa-home',
      sidebarBackground: 'assets/img/sidebar-about-us.jpg',
      showInSidebar: Array(AuthenticationConstants.WAITER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/menu',
      title: 'Menu',
      icon: 'fas fa-utensils',
      sidebarBackground: 'assets/img/sidebar-menu.jpg',
      showInSidebar: Array(AuthenticationConstants.COOK_NAME, AuthenticationConstants.CHEF_NAME,
        AuthenticationConstants.MANAGER_NAME, AuthenticationConstants.WAITER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/reservations',
      title: 'Rezervace',
      icon: 'fas fa-clipboard-list',
      sidebarBackground: 'assets/img/sidebar-reservations.jpg',
      showInSidebar: Array(AuthenticationConstants.WAITER_NAME, AuthenticationConstants.LOGGED_USER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/reservations/new',
      title: 'Rezervace',
      icon: 'far fa-calendar-alt',
      sidebarBackground: 'assets/img/sidebar-reservations.jpg',
      showInSidebar: Array(AuthenticationConstants.UNLOGGED_USER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/reservation',
      title: 'Nová rezervace',
      icon: 'far fa-calendar-alt',
      sidebarBackground: 'assets/img/sidebar-reservations.jpg',
      showInSidebar: Array(AuthenticationConstants.LOGGED_USER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/employees/edit',
      title: 'Správa zaměstnanců',
      icon: 'fas fa-users',
      sidebarBackground: 'assets/img/sidebar-manager.jpg',
      showInSidebar: Array(AuthenticationConstants.MANAGER_NAME),
      showInNavbar: Array()
    },
    {
      path: '/order-items',
      title: 'Objednávky',
      icon: 'fas fa-list-ul',
      sidebarBackground: 'assets/img/sidebar-kitchen.jpg',
      showInSidebar: Array(AuthenticationConstants.COOK_NAME,
        AuthenticationConstants.CHEF_NAME,
        AuthenticationConstants.WAITER_NAME
      ),
      showInNavbar: Array()
    },
    {
      path: '/menu-items',
      title: 'Pokrmy',
      icon: 'fas fa-book',
      sidebarBackground: 'assets/img/sidebar-menu.jpg',
      showInSidebar: Array(AuthenticationConstants.COOK_NAME, AuthenticationConstants.CHEF_NAME),
      showInNavbar: Array()
    },
    {
      path: '/menu-items/new',
      title: 'Nový pokrm',
      icon: 'fas fa-book',
      sidebarBackground: 'assets/img/sidebar-menu.jpg',
      showInSidebar: Array(),
      showInNavbar: Array()
    },
    {
      path: '/menu-items/',
      title: 'Editace pokrmu',
      icon: 'fas fa-book',
      sidebarBackground: 'assets/img/sidebar-menu.jpg',
      showInSidebar: Array(),
      showInNavbar: Array()
    },
    {
      path: '/menu/categories',
      title: 'Kategorie pokrmů',
      icon: 'fas fa-folder-open',
      sidebarBackground: 'assets/img/sidebar-menu.jpg',
      showInSidebar: Array(AuthenticationConstants.CHEF_NAME),
      showInNavbar: Array()
    },
    {
      path: '/sales',
      title: 'Tržby',
      icon: 'far fa-chart-bar',
      sidebarBackground: 'assets/img/sidebar-manager.jpg',
      showInSidebar: Array(AuthenticationConstants.MANAGER_NAME),
      showInNavbar: Array()
    },
  ];

  constructor(private authenticationService: AuthenticationService) {
  }

  isAccessibleByCurrentUser(roles: Array<string>): boolean {
    for (const role of roles) {
      if (role === AuthenticationConstants.UNLOGGED_USER_NAME) {
        if (this.authenticationService.isUserNotLoggedIn()) {
          return true;
        }
      } else {
        if (this.authenticationService.hasUserSpecificRole(role)) {
          return true;
        }
      }
    }
    return false;
  }
}
