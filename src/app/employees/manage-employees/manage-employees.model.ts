export class ChangeRoleStateModel {
  roleName: string;

  constructor(roleName: string) {
    this.roleName = roleName;
  }
}

export interface EditEmployeeAttributeModel {
  attribute: string;
  value: string;
}
