export class NotFoundConstants {
  public static readonly ERROR_MESSAGE = ' Omlouváme se, vyskytla se chyba. Požadovaná stránka nenalezena!';
  public static readonly BACK_BUTTON = 'Zpět na hlavní stránku';
}
