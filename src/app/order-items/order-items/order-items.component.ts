import {Component, OnInit} from '@angular/core';
import {OrderItem} from '../order-items.model';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {OrderItemsService} from '../../../services/order-items/order-items.service';
import {DatePipe} from '@angular/common';
import {ArrayItemManipulatorUtil} from '../../../utils/ArrayItemManipulatorUtil';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {AuthenticationService} from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-cook-chef-order-items',
  templateUrl: './order-items.component.html',
  styleUrls: [
    './order-items.component.less',
    '../order-items.component.less'
  ],
  providers: [DatePipe]
})
export class OrderItemsComponent implements OnInit {

  private orderItemsList: OrderItem[];
  private dataLoaded = false;

  constructor(private orderItemsService: OrderItemsService,
              private datePipe: DatePipe,
              private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.getOrderItemsListData();
  }

  getOrderItemsListData() {
    this.orderItemsService.getOrderItemsListData().subscribe(
      res => {
        this.orderItemsList = res;
        this.deleteFinishedAndPaid();
        this.addStatesInCzech();
        this.addPlaceTypesInCzech();
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  changeState(item: OrderItem) {
    switch (item.state) {
      case 'WAITING': {
        this.makeOrderItemAsPreparing(item);
        break;
      }
      case 'PREPARING': {
        this.makeOrderItemAsFinished(item);
        break;
      }
      case 'FINISHED': {
        this.makeOrderItemAsReleased(item);
        break;
      }
      default: {
        console.log('error - wrong state in order-item');
        break;
      }
    }
  }

  private makeOrderItemAsPreparing(item: OrderItem) {
    if (this.authService.hasUserSpecificRole(this.authService.authConstants.CHEF_NAME) ||
      this.authService.hasUserSpecificRole(this.authService.authConstants.COOK_NAME)) {
      this.orderItemsService.markOrderAsPreparing(item.orderItemId).subscribe(
        res => {
          NotifierDisplayerUtil.showNotification('success', 'Pokrm "' + item.name + '" se připravuje.');
          item.state = 'PREPARING';
          item.stateCzech = 'V přípravě';
        },
        error => {
          ResponseErrorHandler.handleError(error);
        }
      );
    }
  }

  private makeOrderItemAsFinished(item: OrderItem) {
    if (this.authService.hasUserSpecificRole(this.authService.authConstants.CHEF_NAME) ||
      this.authService.hasUserSpecificRole(this.authService.authConstants.COOK_NAME)) {
      this.orderItemsService.markOrderAsFinished(item.orderItemId).subscribe(
        res => {
          NotifierDisplayerUtil.showNotification('success', 'Pokrm "' + item.name + '" je připraven k výdeji.');
          if (this.authService.hasUserSpecificRole(this.authService.authConstants.WAITER_NAME)) {
            item.state = 'FINISHED';
            item.stateCzech = 'Připraveno k výdeji';
          } else {
            ArrayItemManipulatorUtil.deleteItemFromArray(item, this.orderItemsList);
          }
        },
        error => {
          ResponseErrorHandler.handleError(error);
        }
      );
    }
  }

  private makeOrderItemAsReleased(item: OrderItem) {
    if (this.authService.hasUserSpecificRole(this.authService.authConstants.WAITER_NAME)) {
      this.orderItemsService.markOrderAsReleased(item.orderItemId).subscribe(
        res => {
          NotifierDisplayerUtil.showNotification('success', 'Pokrm "' + item.name + '" byl vydán.');
          item.state = 'RELEASED';
          item.stateCzech = 'Vydáno';
        },
        error => {
          ResponseErrorHandler.handleError(error);
        }
      );
    }
  }

  private deleteFinishedAndPaid() {
    if ((this.authService.hasUserSpecificRole(this.authService.authConstants.CHEF_NAME) ||
      this.authService.hasUserSpecificRole(this.authService.authConstants.COOK_NAME)) &&
      !this.authService.hasUserSpecificRole(this.authService.authConstants.WAITER_NAME)) {
      this.orderItemsList = this.orderItemsList.filter(
        item => item.state !== 'FINISHED' && item.state !== 'RELEASED' && item.state !== 'PAID'
      );
    } else {
      this.orderItemsList = this.orderItemsList.filter(item => item.state !== 'PAID');
    }
  }

  private isSameMeal() {
    this.orderItemsList = this.orderItemsList.sort((a: OrderItem, b: OrderItem) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });
  }

  private addStatesInCzech() {
    for (const item of this.orderItemsList) {
      switch (item.state) {
        case 'WAITING': {
          item.stateCzech = 'Čeká na vyřízení';
          break;
        }
        case 'PREPARING': {
          item.stateCzech = 'V přípravě';
          break;
        }
        case 'FINISHED': {
          item.stateCzech = 'Připraveno k výdeji';
          break;
        }
        case 'RELEASED': {
          item.stateCzech = 'Vydáno';
          break;
        }
        case 'PAID': {
          item.stateCzech = 'Zaplaceno';
          break;
        }
        default: {
          console.log('error - wrong state in order-item');
          break;
        }
      }
    }
  }

  private addPlaceTypesInCzech() {
    for (const item of this.orderItemsList) {
      switch (item.placeType) {
        case 'TABLE': {
          item.placeTypeCzech = 'Stůl';
          break;
        }
        case 'LOUNGE': {
          item.placeTypeCzech = 'Salonek';
          break;
        }
        default: {
          console.log('error - wrong place type in order-item');
          break;
        }
      }
    }
  }
}
