export interface OrderItem {
  'orderItemId': number;
  'menuItemId': number;
  'placeId': number;
  'placeType': string;
  'placeTypeCzech': string;
  'createdAt': string;
  'name': string;
  'grammage': string;
  'specialRequirements': string;
  'state': string;
  'stateCzech': string;
}

export interface CategoryOfOrderItems {
  'categoryName': string;
  'orderItems': [
    OrderItemInCategory
  ];
}

export interface CategoryOfMenuItems {
  'categoryName': string;
  'menuItems': [
    MenuItemInCategory
  ];
}

export interface OrderItemInCategory {
  'orderItemId': number;
  'menuItemId': number;
  'name': string;
  'grammage': string;
  'price': number;
  'specialRequirements': string;
  'state': string;
}

export interface MenuItemInCategory {
  'menuItemId': number;
  'name': string;
  'price': number;
  'grammage': string;
  'description': string;
}

