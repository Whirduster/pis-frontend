import {Component, OnInit} from '@angular/core';
import {
  AverageUtilization,
  MenuItemSalesLastTime,
  MenuItemsSalesLastTimeData,
  SoldMenuItem
} from './sales.model';
import {SalesService} from '../../../services/sales/sales.service';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.less']
})
export class SalesComponent implements OnInit {
  Highcharts = Highcharts;

  soldMenuItems: SoldMenuItem[] = [];
  menuItemsSalesLastTime: MenuItemSalesLastTime[] = [];
  averageUtilization: AverageUtilization[] = [];


  countOfSoldMenuItemsChartOptions = {};
  countOfSoldMenuItemsDataReady = false;

  lastTimeSoldChartOptions = {};
  lastTimeSoldDataReady = false;

  averageUtilizationOptions = {};
  averageUtilizationDataReady = false;

  salesOfLast12MonthsChartOptions = {};
  salesOfLast12MonthsDataReady = false;
  salesOfLast12Months: {month: number; saleValue: number}[] = [];

  constructor(private salesService: SalesService) {
  }

  ngOnInit() {
    HC_exporting(Highcharts);
    this.getCountOfSoldMenuItems();
    this.getLastTimeWhenMenuItemWasSold();
    this.getAverageUtilization();
    this.fillMonths();
    this.getSalesInMonth();
  }

  getCountOfSoldMenuItems() {
    this.salesService.getCountOfSoldMenuItems().subscribe(
      res => {
        this.soldMenuItems = res;
        const names = this.soldMenuItems.map(value => value.name + ' ' + value.grammage);
        const counts = this.soldMenuItems.map(value => value.soldCount);
        // const counts = this.soldMenuItems.map(value => {
        //   return {name: value.name + ' ' + value.grammage, y: value.soldCount};
        // });
        this.createGetCountOfSoldMenuItemsChart(names, counts);
        this.countOfSoldMenuItemsDataReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getLastTimeWhenMenuItemWasSold() {
    this.salesService.getLastTimeWhenMenuItemWasSold().subscribe(
      res => {
        this.menuItemsSalesLastTime = res;
        const data: MenuItemsSalesLastTimeData[] = this.menuItemsSalesLastTime.map(value => {
          return {
            name: value.name + ' ' + value.grammage,
            date: new Date(value.lastTimeSold)
          };
        });
        this.createLastTimeSoldChart(data);
        this.lastTimeSoldDataReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getAverageUtilization() {
    this.salesService.getAverageUtilization().subscribe(
      res => {
        this.averageUtilization = res.slice(9);
        this.averageUtilization.push(res[res.length - 1]);
        // TODO
        this.createAverageUtilization();
        this.averageUtilizationDataReady = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getSumOfSalesOfPeriod(year: number, month: number) {
    this.salesService.getSumOfSalesOfPeriod(year, month).subscribe(
      res => {
        this.salesOfLast12Months.find(x => x.month === month).saleValue = res;
        if (this.salesOfLast12Months.length > 11) {
          while (this.salesOfLast12Months[0].month !== new Date().getUTCMonth() + 1) {
            const temp = this.salesOfLast12Months.shift();
            this.salesOfLast12Months.push(temp);
          }
          this.createSalesInLast12Months();
          this.salesOfLast12MonthsDataReady = true;
        }
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  private getSalesInMonth() {
    let year = new Date().getUTCFullYear();
    let month = new Date().getUTCMonth() + 1;

    for (let i = 0; i < 12; i++) {
      this.getSumOfSalesOfPeriod(year, month);
      if (month === 1) {
        year--;
        month = 12;
      } else {
        month--;
      }
    }
  }

  private fillMonths() {
    let year = new Date().getUTCFullYear();
    let month = new Date().getUTCMonth() + 1;
    for (let i = 0; i < 12; i++) {
      this.salesOfLast12Months.push({month, saleValue: null});
      if (month === 1) {
        year--;
        month = 12;
      } else {
        month--;
      }
    }
  }

  private createGetCountOfSoldMenuItemsChart(names, counts) {
    counts.sort((a, b) => a - b);

    this.countOfSoldMenuItemsChartOptions = {
      colors: ['#5aee46', '#3de1e9', '#8d4654', '#bbbf36', '#aaeeee',
        '#584eff', '#f47814', '#7f7ebf', '#df819e', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: null,
        style: {
          fontFamily: 'Verdana, serif'
        }
      },
      title: {
        style: {
          color: 'black',
          fontSize: '16px',
          fontWeight: 'bold'
        },
        text: 'Počet prodaných pokrmů'
      },
      subtitle: {
        text: 'pouze zaplacené objednávky'
      },
      tooltip: {
        borderWidth: 0
      },
      labels: {
        style: {
          color: '#6e6e70'
        }
      },
      legend: {
        backgroundColor: '#E0E0E8',
        itemStyle: {
          fontWeight: 'bold',
          fontSize: '13px'
        }
      },
      xAxis: {
        labels: {
          style: {
            color: '#6e6e70'
          }
        },
        categories: names
      },
      yAxis: {
        allowDecimals: false,
        title: {
          enabled: false
        },
        labels: {
          format: '{value} ks',
          style: {
            color: '#6e6e70'
          }
        }
      },
      plotOptions: {
        series: {
          shadow: true
        },
        candlestick: {
          lineColor: '#404048'
        },
        map: {
          shadow: false
        }
      },
      navigator: {
        xAxis: {
          gridLineColor: '#D0D0D8'
        }
      },
      rangeSelector: {
        buttonTheme: {
          fill: 'white',
          stroke: '#C0C0C8',
          'stroke-width': 1,
          states: {
            select: {
              fill: '#D0D0D8'
            }
          }
        }
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8'
      },
      exporting: {
        enabled: true
      },
      series: [{
        type: 'column',
        name: 'počet',
        colorByPoint: true,
        data: counts,
        showInLegend: false
      }]
    };
  }

  private createLastTimeSoldChart(data: MenuItemsSalesLastTimeData[]) {
    data.sort((a, b) => a.date.getTime() - b.date.getTime());
    const names = data.map(value => value.name);

    const now = new Date();
    const dates = data.map(value => (now.getTime() - value.date.getTime()) / 1000 / 60 / 60 / 24);

    this.lastTimeSoldChartOptions = {
      colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee',
        '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
        backgroundColor: null,
        style: {
          fontFamily: 'Verdana, serif'
        }
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8'
      },
      exporting: {
        enabled: true
      },
      title: {
        style: {
          color: 'black',
          fontSize: '16px',
          fontWeight: 'bold'
        },
        text: 'Počet dnů od posledního objednání pokrmů'
      },
      subtitle: {
        text: 'pokrmy, které nikdy nebyly objednány nejsou v grafu obsaženy'
      },
      labels: {
        style: {
          color: '#6e6e70'
        }
      },
      legend: {
        backgroundColor: '#E0E0E8',
        itemStyle: {
          fontWeight: 'bold',
          fontSize: '13px'
        }
      },
      tooltip: {
        borderWidth: 0,
        pointFormat: 'dnů: {point.y:.1f}'
      },
      xAxis: {
       categories: names
      },
      yAxis: {
        title: {
          enabled: false
        },
        labels: {
          format: '{value} dní'
        }
      },
      series: [
        {
          type: 'column',
          name: 'dnů',
          colorByPoint: true,
          data: dates,
          showInLegend: false
        }
        ],
      plotOptions: {
        series: {
          shadow: true
        },
        candlestick: {
          lineColor: '#404048'
        },
        map: {
          shadow: false
        }
      }
    };
  }

  private createAverageUtilization() {
    this.averageUtilizationOptions = {
      colors: ['#ee646c'],
      chart: {
        type: 'area',
        backgroundColor: null,
        style: {
          fontFamily: 'Verdana, serif'
        }
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8'
      },
      exporting: {
        enabled: true
      },
      title: {
        style: {
          color: 'black',
          fontSize: '16px',
          fontWeight: 'bold'
        },
        text: 'Průměrná denní návštěvnost'
      },
      xAxis: {
        categories: ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00',
          '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '0:00'],
        tickmarkPlacement: 'on',
        title: {
          enabled: false
        }
      },
      yAxis: {
        title: {
          text: '% z denní návštěvnosti'
        },
        labels: {
          format: '{value}%'
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        split: true,
        valueSuffix: '%'
      },
      series: [{
        name: '',
        data: this.averageUtilization
      }]
    };

  }

  private createSalesInLast12Months() {
    this.salesOfLast12MonthsChartOptions = {
      chart: {
        type: 'line',
        backgroundColor: null,
        style: {
          fontFamily: 'Verdana, serif'
        }
      },
      exporting: {
        enabled: true
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8'
      },
      title: {
        text: 'Zisk za poslední rok',
        style: {
          color: 'black',
          fontSize: '16px',
          fontWeight: 'bold'
        }
      },
      subtitle: {
        text: 'v horizontu měsíců',
        style: {
          color: 'black'
        }
      },
      tooltip: {
        borderWidth: 0
      },
      labels: {
        style: {
          color: '#6e6e70'
        }
      },
      xAxis: {
        categories: this.salesOfLast12Months.map(value => {
          if (value.month <= new Date().getUTCMonth() + 1) {
            return value.month + '/2020';
          } else {
            return value.month + '/2019';
          }
        }).reverse(),
        title: {
          enabled: false
        }
      },
      yAxis: {
        title: {
          text: 'Kč/měsíc'
        },
        labels: {
          format: '{value} Kč'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true,
            format: '{y} Kč'
          },
          enableMouseTracking: false
        }
      },
      series: [{
        data: this.salesOfLast12Months.map(value => value.saleValue).reverse()
      }]
    };
  }
}

