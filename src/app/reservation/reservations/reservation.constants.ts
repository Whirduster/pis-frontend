export class ReservationConstants {
  public static readonly PLACE_TABLE = 'Stůl';
  public static readonly PLACE_LOUNGE = 'Salónek';
  public static readonly CANCELED_RESERVATION = 'Zrušená rezervace';
}
