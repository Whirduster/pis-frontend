import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {

  constructor(public router: Router) {

  }

  isUnloggedPage = new RegExp('(^/(homepage|login|register|reservations/new)$|^/(homepage|login|register|reservations/new)[\\/?#].*)');

  ngOnInit() {
  }
}
