import {Md5} from 'ts-md5';

export class AuthenticationConstants {

  constructor() {
  }

  public static LOGGED_EMAIL = 'loggedEmail';

  /* names of employees */
  public static readonly WAITER_NAME = 'Waiter';
  public static readonly COOK_NAME = 'Cook';
  public static readonly CHEF_NAME = 'Chef';
  public static readonly MANAGER_NAME = 'Manager';
  public static readonly LOGGED_USER_NAME = 'Logged user';
  public static readonly UNLOGGED_USER_NAME = 'Unlogged user';

  /* hashes for employees */
  private static WAITER_HASH = AuthenticationConstants.generateHash(AuthenticationConstants.WAITER_NAME);
  private static COOK_HASH = AuthenticationConstants.generateHash(AuthenticationConstants.COOK_NAME);
  private static CHEF_HASH = AuthenticationConstants.generateHash(AuthenticationConstants.CHEF_NAME);
  private static MANAGER_HASH = AuthenticationConstants.generateHash(AuthenticationConstants.MANAGER_NAME);
  public static LOGGED_USER_HASH = AuthenticationConstants.generateHash(AuthenticationConstants.LOGGED_USER_NAME);

  public static readonly EMPLOYEE_NAMES = Array(
    AuthenticationConstants.WAITER_NAME,
    AuthenticationConstants.COOK_NAME,
    AuthenticationConstants.CHEF_NAME,
    AuthenticationConstants.MANAGER_NAME
  );

  public static readonly USER_NAMES = Array(
    AuthenticationConstants.LOGGED_USER_NAME,
    AuthenticationConstants.UNLOGGED_USER_NAME
  );

  public static readonly ALL_NAMES = AuthenticationConstants.EMPLOYEE_NAMES.concat(AuthenticationConstants.USER_NAMES);

  /* summary */
  public static readonly USER_NAME_HASH_ARRAY = [
    {
      name: AuthenticationConstants.WAITER_NAME,
      hash: AuthenticationConstants.WAITER_HASH
    },
    {
      name: AuthenticationConstants.COOK_NAME,
      hash: AuthenticationConstants.COOK_HASH
    },
    {
      name: AuthenticationConstants.CHEF_NAME,
      hash: AuthenticationConstants.CHEF_HASH
    },
    {
      name: AuthenticationConstants.MANAGER_NAME,
      hash: AuthenticationConstants.MANAGER_HASH
    },
    {
      name: AuthenticationConstants.LOGGED_USER_NAME,
      hash: AuthenticationConstants.LOGGED_USER_HASH
    }
  ];

  private static generateHash(name: string): string {
    const secret = 'secretPIS';
    return Md5.hashStr(name + secret).toString();
  }

}
