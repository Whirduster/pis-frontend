import {Component, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';
import {RegisterConstants} from './register.constants';
import {RegisterModel} from './register.model';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.less',
    '../unlogged-user-pages.component.less'
  ]
})
export class RegisterComponent implements OnInit {
  private readonly registerConstants: typeof RegisterConstants = RegisterConstants;
  private toggleButton: any;
  private sidebarVisible: boolean;
  private windowScrolled: boolean;

  repeatedPasswordModel: '';
  user: RegisterModel = {
    name: '',
    surname: '',
    email: '',
    password: '',
  };


  constructor(private authenticationService: AuthenticationService, private router: Router,
              private renderer: Renderer2, private el: ElementRef) {
  }

  ngOnInit() {
    const navbar: HTMLElement = this.el.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
  }

  sendRegisterData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    this.authenticationService.sendRegisterData(this.user).subscribe(
      res => {
        localStorage.clear();
        localStorage.setItem('token', res.token);
        localStorage.setItem(this.authenticationService.authConstants.LOGGED_USER_NAME,
                               this.authenticationService.authConstants.LOGGED_USER_HASH);
        localStorage.setItem(this.authenticationService.authConstants.LOGGED_EMAIL,
                               this.user.email);
        this.router.navigate(['/reservations']);
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  arePasswordsSame() {
    return this.repeatedPasswordModel.toString() === this.user.password.toString();
  }

  sidebarOpen() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.addClass(this.toggleButton, 'toggled');
    this.renderer.addClass(nav, 'nav-open');
    this.sidebarVisible = true;
  }

  sidebarClose() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.removeClass(this.toggleButton, 'toggled');
    this.renderer.removeClass(nav, 'nav-open');
    this.sidebarVisible = false;
  }

  toggleSidebar() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  isTablet() {
    return window.innerWidth <= 991;
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = document.documentElement.scrollTop || 0;
    this.windowScrolled = scrollPosition >= 20;
  }
}
