import {Component, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';
import {CarouselImageModel} from './home-page.model';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {NavigationRoutesService, RouteInfo} from '../../../services/main-fragments/navigation-routes.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication/authentication.service';

declare var $: any;

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: [
    '../unlogged-user-pages.component.less',
    '../../main-fragments/footer/footer.component.less'
  ]
})
export class HomePageComponent implements OnInit {
  private toggleButton: any;
  private sidebarVisible: boolean;
  private windowScrolled: boolean;
  private navbarItems: Array<RouteInfo>;
  private topPosToStartShowing = 300;
  private parallaxSection;

  constructor(config: NgbCarouselConfig, private el: ElementRef, private renderer: Renderer2,
              private routes: NavigationRoutesService, private router: Router,
              private authService: AuthenticationService) {
    config.interval = 10000;
    config.pauseOnHover = false;
  }

  carouselRestaurant: CarouselImageModel[] = [
    {src: 'https://restauracenachate.cz/pozadi-restaurace/1.jpg', label: '', caption: ''},
    {src: 'https://restauracenachate.cz/pozadi-restaurace/2.jpg', label: '', caption: ''},
    {src: 'https://restauracenachate.cz/pozadi-restaurace/3.jpg', label: '', caption: ''},
    {src: 'https://restauracenachate.cz/pozadi-restaurace/4.jpg', label: '', caption: ''},
    {src: 'https://restauracenachate.cz/pozadi-restaurace/5.jpg', label: '', caption: ''},
    {src: 'https://restauracenachate.cz/pozadi-restaurace/6.jpg', label: '', caption: ''},
  ];

  carouselFood: CarouselImageModel[] = [
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111551.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111558.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111615.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111632.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111651.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111705.jpg', label: '', caption: ''},
    {src: 'http://www.restauracelednice.cz/images/gallery/20150323111714.jpg', label: '', caption: ''}
  ];


  ngOnInit() {
    const navbar: HTMLElement = this.el.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    this.checkScroll();
    this.navbarItems = this.routes.routes;
  }

  sidebarOpen() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.addClass(this.toggleButton, 'toggled');
    this.renderer.addClass(nav, 'nav-open');
    this.sidebarVisible = true;
  }

  sidebarClose() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.removeClass(this.toggleButton, 'toggled');
    this.renderer.removeClass(nav, 'nav-open');
    this.sidebarVisible = false;
  }

  toggleSidebar() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = document.documentElement.scrollTop || 0;
    this.windowScrolled = scrollPosition >= this.topPosToStartShowing;
    this.parallax(scrollPosition);
  }

  scrollToTop() {
    document.getElementsByTagName('html').item(0).scroll({
      top: 0,
      behavior: 'smooth'
    });
  }

  scroll(element: HTMLElement) {
    element.scrollIntoView({behavior: 'smooth'});
  }

  isTablet() {
    return window.innerWidth <= 991;
  }

  private parallax(scrollPosition) {
    this.parallaxSection = $('.page-header[data-parallax="true"]');
    const oVal = scrollPosition / 3;
    const styles = {
      transform: 'translate3d(0,' + oVal + 'px,0)',
      '-webkit-transform': 'translate3d(0,' + oVal + 'px,0)',
      '-ms-transform': 'translate3d(0,' + oVal + 'px,0)',
      '-o-transform': 'translate3d(0,' + oVal + 'px,0)'
    };
    this.parallaxSection.css(styles);
  }

  logout() {
    if (localStorage.length === 0) {
      this.router.navigate(['/login']);
    } else {
      localStorage.clear();
      this.router.navigate(['/homepage']);
    }
  }
}
