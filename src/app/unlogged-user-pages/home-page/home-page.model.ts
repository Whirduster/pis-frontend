export interface CarouselImageModel {
  src: string;
  label: string;
  caption: string;
}
