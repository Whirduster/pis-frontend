export class LoginConstants {

  public static readonly REQUIRED_FIELD = 'Toto pole je povinné';
  public static readonly LOG_IN_BUTTON = 'Přihlásit se';
  public static readonly WRONG_EMAIL_FORMAT = 'Zadejte email ve správném tvaru';
}
