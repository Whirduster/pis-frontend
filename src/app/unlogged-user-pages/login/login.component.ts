import {Component, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';
import {LoginModel} from './login.model';
import {LoginConstants} from './login.constants';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {LoginResponseModel} from '../../response.model';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.less',
    '../unlogged-user-pages.component.less'
  ]
})
export class LoginComponent implements OnInit {
  private readonly loginConstants: typeof LoginConstants = LoginConstants;
  private toggleButton: any;
  private sidebarVisible: boolean;
  private windowScrolled: boolean;

  loginModel: LoginModel = {
    email: '',
    password: ''
  };

  constructor(private authenticationService: AuthenticationService, private router: Router,
              private renderer: Renderer2, private el: ElementRef) {}
  ngOnInit() {
    const navbar: HTMLElement = this.el.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
  }

  sendLoginData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    this.authenticationService.sendLoginData(this.loginModel).subscribe(
      res => {
        this.fillLocalStorage(res, this.loginModel.email);
        this.authenticationService.getHomepage(this.router);
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }


 private fillLocalStorage(user: LoginResponseModel, email: string) {
   localStorage.clear();
   localStorage.setItem(this.authenticationService.authConstants.LOGGED_EMAIL, email);
   localStorage.setItem('token', user.token);

   for (const entry of this.authenticationService.authConstants.USER_NAME_HASH_ARRAY) {
      if (user.roles.find(s  => s === entry.name)) {
        localStorage.setItem(entry.name, entry.hash);
        if (entry.name === this.authenticationService.authConstants.LOGGED_USER_NAME) {
          return;
        }
      }
    }
  }

  sidebarOpen() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.addClass(this.toggleButton, 'toggled');
    this.renderer.addClass(nav, 'nav-open');
    this.sidebarVisible = true;
  }

  sidebarClose() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.removeClass(this.toggleButton, 'toggled');
    this.renderer.removeClass(nav, 'nav-open');
    this.sidebarVisible = false;
  }

  toggleSidebar() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  isTablet() {
    return window.innerWidth <= 991;
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = document.documentElement.scrollTop || 0;
    this.windowScrolled = scrollPosition >= 20;
  }
}
