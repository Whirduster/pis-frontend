export class BillingConstants {
  public static readonly BILL = 'Účet';
  public static readonly EMPTY_BILL = 'Žádné pokrmy k zaplacení';
  public static readonly TABLE = 'Stůl';
  public static readonly EMPTY_TABLE = 'Žádné objednávky na tento stůl';
  public static readonly BUTTON_PAY = 'Zaplatit';
}
