export class MenuConstants {

  /* categories */
  public static readonly NO_CATEGORIES_IN_DB = 'V databázi nejsou žádné kategorie pokrmů.';
  public static readonly CATEGORY = 'Kategorie pokrmů';
  public static readonly ADD_CATEGORY = 'Přidat kategorii';
  public static readonly RESTART = 'Zrušit';
  public static readonly SAVE = 'Uložit';

  /* menu items */
  public static readonly NO_MENU_ITEMS_IN_DB = 'V databázi nejsou žádné pokrmy.';
  public static readonly MENU_ITEMS = 'Pokrmy';
  public static readonly ADD_MENU_ITEM = 'Přidat nový pokrm';

  /* new menu item */
  public static readonly NO_CATEGORIES_CREATE_SOME = 'V databázi nejsou žádné kategorie pokrmů, nejprve nějakou vytvořte.';
  public static readonly REQUIRED_FIELD = 'Toto pole je povinné';

  /* menu */
  public static readonly EMPTY_NOT_IN_MENU = 'Žádná jídla nejsou k dispozici';

  /* edit menu item */
  public static readonly EDIT_MENU_ITEM = 'Editace pokrmu';

  /* normal menu */
  public static readonly MENU = 'Menu';
  public static readonly EMPTY_MENU = 'Momentálně je naše menu prázdné.';
  public static readonly NAME = 'Název';
  public static readonly DESCRIPTION = 'Popisek';
  public static readonly GRAMMAGE = 'Gramáž (g) / objem (l)';
  public static readonly PRICE = 'Cena';
}
