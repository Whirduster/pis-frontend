import {AfterViewChecked, Component, HostListener, OnInit} from '@angular/core';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {MenuService} from '../../../services/menu/menu.service';
import {MenuConstants} from '../menu.constants';
import {NewCategoryModel} from '../menu/chef-menu/chef-menu.model';
import {ArrayItemManipulatorUtil} from '../../../utils/ArrayItemManipulatorUtil';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import Swal from 'sweetalert2';
import {SweetalertInfoSetterUtil} from '../../../utils/SweetalertInfoSetterUtil';
import {ActionButtonUtil} from '../../../utils/ActionButtonUtil';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: [
    './category.component.less',
    '../menu.less'
  ]
})
export class CategoryComponent implements OnInit, AfterViewChecked {
  private menuConstants: typeof MenuConstants = MenuConstants;
  private categories = Array();
  private newCategoryName: '';
  private dataLoaded = false;

  constructor(private menuService: MenuService) {
  }

  ngOnInit() {
    this.getAllCategories();
  }

  ngAfterViewChecked() {
    ActionButtonUtil.setStyle();
  }

  getAllCategories() {
    this.menuService.getAllCategories().subscribe(
      res => {
        for (const category of res) {
          this.categories.push(category.name);
        }
        this.dataLoaded = true;
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  sendAddCategory() {
    if (this.newCategoryName === undefined || this.newCategoryName === '') {
      NotifierDisplayerUtil.showNotification('danger', 'Vyplňte prosím pole názvem kategorie pokrmů.');
      return;
    }

    if (this.categories.find(x => x === this.newCategoryName)) {
      NotifierDisplayerUtil.showNotification('danger', 'Tato kategorie pokrmů již existuje.');
      return;
    }

    const newCategoryModel: NewCategoryModel = {
      name: this.newCategoryName
    };

    this.menuService.addCategory(newCategoryModel).subscribe(
      res => {
        this.categories.push(this.newCategoryName);
        NotifierDisplayerUtil.showNotification('success', 'Kategorie pokrmů úspěšně přidána.');
        this.newCategoryName = '';
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  sendDeleteCategory(name: string) {
    Swal.fire(
      SweetalertInfoSetterUtil.getInfoForDeleteAlert('Tímto odstraníte i pokrmy z této kategorie!' +
        ' Opravdu chcete vymazat kategorii pokrmů "' + name + '"?')
    ).then((result) => {
      if (result.value) {
        this.menuService.deleteCategory(name).subscribe(
          res => {
            ArrayItemManipulatorUtil.deleteItemFromArray(name, this.categories);
            NotifierDisplayerUtil.showNotification('success', 'Kategorie pokrmů "' + name + '" úspěšně odebrána.');
          },
          error => {
            ResponseErrorHandler.handleError(error);
          }
        );
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    ActionButtonUtil.setStyle();
  }
}
