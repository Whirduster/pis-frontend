export class NewAndEditMenuItem {
  'name': string;
  'price': number;
  'productionPrice': number;
  'grammage': string;
  'description': string;
  'categoryId': number;
}

export class MenuItem {
  'menuItemId': number;
  'name': string;
  'price': number;
  'productionPrice': number;
  'grammage': string;
  'description': string;
  'categoryName': string;
  'inMenu': boolean;
}
