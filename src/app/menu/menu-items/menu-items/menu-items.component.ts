import {AfterViewChecked, Component, HostListener, OnInit} from '@angular/core';
import {MenuConstants} from '../../menu.constants';
import {MenuService} from '../../../../services/menu/menu.service';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {AuthenticationService} from '../../../../services/authentication/authentication.service';
import {ArrayItemManipulatorUtil} from '../../../../utils/ArrayItemManipulatorUtil';
import {NotifierDisplayerUtil} from '../../../../utils/NotifierDisplayerUtil';
import {Category, MenuItemInCategory} from '../../menu/chef-menu/chef-menu.model';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {SweetalertInfoSetterUtil} from '../../../../utils/SweetalertInfoSetterUtil';
import {ActionButtonUtil} from '../../../../utils/ActionButtonUtil';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: [
    './menu-items.component.less',
    '../../menu.less'
  ]
})
export class MenuItemsComponent implements OnInit, AfterViewChecked {
  private menuConstants: typeof MenuConstants = MenuConstants;
  private categories: Category[] = [];
  private isButtonsVertical = false;
  private dataLoaded = false;

  constructor(private menuService: MenuService,
              private authService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
    this.getAllMenuItems();
  }

  ngAfterViewChecked() {
    this.onResize();
  }

  getAllMenuItems() {
    this.menuService.getAllMenuItems().subscribe(
      res => {
        this.categories = this.deleteEmptyCategories(res);
        this.dataLoaded = true;
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  sendDeleteMenuItem(menuItem: MenuItemInCategory, category: Category) {
    Swal.fire(
      SweetalertInfoSetterUtil.getInfoForDeleteAlert('Opravdu chcete vymazat pokrm "' + menuItem.name + '"?')
    ).then((result) => {
      if (result.value) {
        this.menuService.deleteMenuItem(menuItem.menuItemId).subscribe(
          res => {
            ArrayItemManipulatorUtil.deleteItemFromArray(menuItem, category.menuItems);
            this.categories = this.deleteEmptyCategories(this.categories);
            NotifierDisplayerUtil.showNotification('success', 'Pokrm úspěšně odebrán.');
          },
          error => {
            ResponseErrorHandler.handleError(error);
          }
        );
      }
    });
  }

  private navigateToNewMenuItem() {
    this.router.navigate(['/menu-items/new']);
  }

  private navigateToEditMenuItem(menuItem: MenuItemInCategory) {
    const id = menuItem.menuItemId;
    this.router.navigate(['/menu-items/' + id]);
  }


  private deleteEmptyCategories(categories: Category[]): Category[] {
    return categories.filter(x => x.menuItems.length > 0);
  }

  showPopupMenuItemInfo(menuItemId: number) {
    this.menuService.getMenuItem(menuItemId).subscribe(
      res => {
        Swal.fire(SweetalertInfoSetterUtil.getMenuItemFullInfoAlert(res));
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    const actionButtonTds = (document.getElementsByClassName('action-button') as HTMLCollectionOf<HTMLElement>);
    let maxWidth = -1;
    for (const actionButtonTd of actionButtonTds) {
      ActionButtonUtil.unsetStyle(actionButtonTd);
      const parentTr = actionButtonTd.parentElement;
      maxWidth = Math.max(maxWidth, parentTr.clientWidth);
    }
    if (this.authService.hasUserSpecificRole(this.authService.authConstants.CHEF_NAME)) {
      if (maxWidth < 468 && maxWidth > 0) {
        this.makeButtonsVertical(actionButtonTds);
        this.isButtonsVertical = true;
      } else if (maxWidth >= 468) {
        this.makeButtonsHorizontal(actionButtonTds);
        this.isButtonsVertical = false;
      }
    }
    if (!this.isButtonsVertical) {
      ActionButtonUtil.setStyle();
    }
  }

  makeButtonsVertical(actionButtonTds: HTMLCollectionOf<HTMLElement>) {
    for (const actionButtonTd of actionButtonTds) {
      actionButtonTd.classList.add('buttons-vertical');
    }
  }

  makeButtonsHorizontal(actionButtonTds: HTMLCollectionOf<HTMLElement>) {
    for (const actionButtonTd of actionButtonTds) {
      actionButtonTd.classList.remove('buttons-vertical');
    }
  }
}
