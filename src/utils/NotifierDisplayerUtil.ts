declare var $: any;

export class NotifierDisplayerUtil {

  static showNotification(type: string, message: string) {
    if (type === 'primary') {
      this.showNotificationWithOwnIcon(type, 'fas fa-bell', message);
    } else if (type === 'info') {
      this.showNotificationWithOwnIcon(type, 'fas fa-info', message);
    } else if (type === 'success') {
      this.showNotificationWithOwnIcon(type, 'fas fa-check', message);
    } else if (type === 'warning') {
      this.showNotificationWithOwnIcon(type, 'fas fa-exclamation', message);
    } else if (type === 'danger') {
      this.showNotificationWithOwnIcon(type, 'fas fa-exclamation-triangle', message);
    } else {
      console.error('Wrong notification type: ' + type);
    }
  }

  static showNotificationWithOwnIcon(type: string, icon: string, message: string) {
    message = '<b>' + message + '</b>';
    $.notify({
      icon,
      message
    }, {
      type,
      delay: 3000,
      placement: {
        from: 'bottom',
        align: 'left'
      },
      newest_on_top: true,
      animate: {
        enter: 'animated slideInLeft faster',
        exit: 'animated slideOutLeft faster'
      },
      mouse_over: 'pause',
      dismissOnClick: true,
      allow_dismiss: false
    });
  }
}
