import {NewAndEditMenuItem} from '../app/menu/menu-items/menu-items.model';
import {NotifierDisplayerUtil} from './NotifierDisplayerUtil';

export class ValuesCheckerUtil {

  static checkPositivePrice(menuItem: NewAndEditMenuItem) {
    if (menuItem.price > 0 && menuItem.productionPrice > 0) {
      return true;
    } else {
      NotifierDisplayerUtil.showNotification('danger', 'Zadané číselné hodnoty musí být kladná čísla.');
      return false;
    }
  }

  static checkEmptyGrammage(menuItem: NewAndEditMenuItem) {
    if (menuItem.grammage.length > 1 && (menuItem.grammage.includes('g') || menuItem.grammage.includes('l'))) {
      return true;
    } else {
      NotifierDisplayerUtil.showNotification('danger', 'Gramáž musí být zadána ve tvaru číslo a měrná jednotka.');
      return false;
    }
  }
}
