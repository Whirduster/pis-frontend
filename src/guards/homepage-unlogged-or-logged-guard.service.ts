import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HomepageUnloggedOrLoggedGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  canActivate(): boolean {
    if (this.authService.isUserNotLoggedIn() ||
      this.authService.hasUserSpecificRole(this.authService.authConstants.LOGGED_USER_NAME)) {
      return true;
    }
    this.authService.getHomepage(this.router);
    return false;
  }
}
