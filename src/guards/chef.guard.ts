import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ChefGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  canActivate(): boolean {
    if (this.authService.hasUserSpecificRole(this.authService.authConstants.CHEF_NAME)) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
